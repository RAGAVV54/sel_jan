package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC002_CreateLead extends SeleniumBase
{
	
	@Test (groups="smoke")
	@Parameters({"url","username","password"})
	public void create() {
		startApp("chrome", "url");
		
		WebElement username = locateElement("id", "username");
		clearAndType(username, "username");
		WebElement password = locateElement("id", "password");
		clearAndType(password, "password");
		WebElement submit = locateElement("class", "decorativeSubmit");
		click(submit);
		WebElement crm = locateElement("LinkText", "CRM/SFA");
		click(crm);
		WebElement createlead = locateElement("LinkText", "Create Lead");
		click(createlead);
		WebElement companyname = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyname, "HCL");
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstname, "SIDDHARTH");
		WebElement lastname = locateElement("xpath", "//input[@id='createLeadForm_lastName']");
		clearAndType(lastname, "Saranath");
		WebElement submit1 = locateElement("xpath", "//input[@name='submitButton']");
		click(submit1);
		WebElement merge = locateElement("xpath", "//a[text()='Merge Leads']");
		click(merge);
		}
}

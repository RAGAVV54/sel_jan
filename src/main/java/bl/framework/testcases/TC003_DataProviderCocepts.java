	package bl.framework.testcases;

	import org.openqa.selenium.WebElement;
	import org.testng.annotations.DataProvider;
	import org.testng.annotations.Parameters;
	import org.testng.annotations.Test;

	import bl.framework.api.SeleniumBase;

	public class TC003_DataProviderCocepts extends SeleniumBase
	{
		
		@Test (dataProvider = "getdata1")
		
		public void create() {
			startApp("chrome", "http://leaftaps.com/opentaps/control/main");
			String[] Cname,Fname,Lname;
			WebElement username = locateElement("id", "username");
			clearAndType(username, "username");
			WebElement password = locateElement("id", "password");
			clearAndType(password, "password");
			WebElement submit = locateElement("class", "decorativeSubmit");
			click(submit);
			WebElement crm = locateElement("LinkText", "CRM/SFA");
			click(crm);
			WebElement createlead = locateElement("LinkText", "Create Lead");
			click(createlead);
			WebElement companyname = locateElement("id", "createLeadForm_companyName");
			clearAndType(Cname);
			WebElement firstname = locateElement("id", "createLeadForm_firstName");
			clearAndType(Fname);
			WebElement lastname = locateElement("xpath", "//input[@id='createLeadForm_lastName']");
			clearAndType(Lname);
			WebElement submit1 = locateElement("xpath", "//input[@name='submitButton']");
			click(submit1);
			WebElement merge = locateElement("xpath", "//a[text()='Merge Leads']");
			click(merge);
			}
		private void clearAndType(String cname) {
			// TODO Auto-generated method stub
			
		}
		@DataProvider(name="getdata1")
		public String[][] fetchData() {
		String [][] data = new String[2][3];
		
		data[0][1]="TestLeaf";
		data[0][2]= "Koushik";
		data[0][3]= "C";
		
		data [1][1]="TestLeaf";
		data[1][2]="Gayathri";
		data[1][3]="C";
		return data;
				}
		@DataProvider(name="getdata2")
		public String[][] fetchData1() {
		String [][] data = new String[2][3];
		
		data[0][1]="TestLeaf";
		data[0][2]= "Balaji";
		data[0][3]= "A";
		
		data [1][1]="TestLeaf";
		data[1][2]="Gopi";
		data[1][3]="A";
		return data;
				}
		}
